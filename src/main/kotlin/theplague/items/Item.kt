package theplague.items

import theplague.interfaces.Iconizable
import theplague.items.player.vehicle.OnFoot

abstract class Item() : Iconizable{
    abstract var timesLeft:Int
    fun use() {
        timesLeft --
    }
}