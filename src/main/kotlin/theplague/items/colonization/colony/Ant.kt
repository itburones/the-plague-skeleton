package theplague.items.colonization.colony

import theplague.interfaces.Position
import theplague.items.colonization.Colonization
import theplague.items.player.weapon.Broom
import theplague.items.player.weapon.Hand
import theplague.items.player.weapon.Sword
import theplague.items.player.weapon.Weapon
import theplague.logic.Territory

const val reproductionTax = 3
class Ant() : Colony() {

    override var size: Int = 1
    override val icon = "\uD83D\uDC1C"

    override fun willReproduce(): Boolean {
        return size < max
    }

    override fun reproduce() {
        if((1..10).random() in 1..3){
            size += if(willReproduce()) 1 else 0
        }
    }

    override fun needsToExpand():Boolean {
        return size == max
    }

    override fun attacked(weapon: Weapon) {
        when(weapon){
            is Broom -> size = 0
            is Sword -> size -= 1
            else -> size -= 2
        }
    }

    override fun colonizedBy(plague: Colony?): Colony? {
        if(plague is Ant){
            plague!!.size ++
            return plague
        } else if (plague is Dragon) {
            return plague
        } else {
            return Ant()
        }
    }

    override fun expand(position: Position, maxPosition: Position): List<Colonization> {
        val result = mutableListOf<Colonization>()
        for(i in position.y-1..position.y+1){
            if(i <= maxPosition.y && i >= 0){
                for(j in position.x-1..position.x+1){
                    if(j <= maxPosition.x && j >= 0){
                        if (position.x == j && position.y == i) {

                        }else{
                            result.add(Colonization(Ant(), Position(j, i)))
                        }
                    }
                }
            }
        }
        return result
    }

    override fun colonizableTo(territory: Territory): Boolean {
        return (territory.colony is Ant && territory.colony!!.size < max) || territory.colony == null
    }

}