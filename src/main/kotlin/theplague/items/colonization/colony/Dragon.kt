package theplague.items.colonization.colony

import androidx.compose.runtime.internal.isLiveLiteralsEnabled
import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import theplague.items.colonization.Colonization
import theplague.items.player.weapon.Broom
import theplague.items.player.weapon.Sword
import theplague.items.player.weapon.Weapon
import theplague.logic.Territory

const val timeToReproduce = 5

class Dragon() : Colony() {

    override var size: Int = 1
    override var icon = "\uD83D\uDC09"
    var lived = 0

    override fun willReproduce(): Boolean {
        lived++
        return lived >= timeToReproduce && size < max
    }

    override fun reproduce() {
        if(willReproduce()){
            size++
            lived = 0
        }
    }

    override fun needsToExpand():Boolean {
        return size == max
    }

    override fun attacked(weapon: Weapon) {
        size -= if(weapon is Sword) 1 else 0
    }

    override fun colonizedBy(plague: Colony?): Colony? {
        if(plague is Dragon) {
            plague.size++
            return plague
        }
        return Dragon()
    }

    override fun expand(position: Position, maxPosition: Position): List<Colonization> {
        val result = mutableListOf<Colonization>()
        for(i in 0..maxPosition.y){
            for(j in 0..maxPosition.x){
                if (position.x == j && position.y == i) {

                }else{
                    result.add(Colonization(Dragon(), Position(j, i)))
                }
            }
        }
        return result
    }

    override fun colonizableTo(territory: Territory):Boolean{
        return (territory.colony is Dragon && territory.colony!!.size < max) || territory.colony is Ant || territory.colony == null
    }

}