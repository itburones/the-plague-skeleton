package theplague.items.colonization.colony

import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import theplague.items.colonization.Colonization
import theplague.items.player.weapon.Weapon
import theplague.logic.Territory

abstract class Colony(): Iconizable {

  val max = 3

  abstract var size:Int

  abstract fun willReproduce():Boolean

  abstract fun reproduce()

  abstract fun needsToExpand():Boolean

  abstract fun attacked(weapon: Weapon)

  abstract fun colonizedBy(plague: Colony?): Colony?

  abstract fun expand(position: Position, maxPosition: Position): List<Colonization>

  abstract fun colonizableTo(territory: Territory):Boolean

}