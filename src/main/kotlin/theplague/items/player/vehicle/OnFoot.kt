package theplague.items.player.vehicle

import theplague.interfaces.Position
import kotlin.math.abs

class OnFoot() : Vehicle() {
    override var timesLeft: Int = -1
    override val icon: String = "\uD83D\uDEB6"

    override fun canMove(from: Position, to: Position): Boolean {
        return abs(from.y - to.y) <= 1 && abs(from.x - to.x) <= 1
    }
}