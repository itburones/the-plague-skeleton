package theplague.items.player.vehicle

import theplague.interfaces.Position

class Helicopter(): Vehicle() {
    override var timesLeft: Int = 5
    override val icon: String = "\uD83D\uDE81"

    override fun canMove(from: Position, to: Position): Boolean {
        return true
    }
}