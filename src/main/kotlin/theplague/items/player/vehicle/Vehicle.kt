package theplague.items.player.vehicle

import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import theplague.items.Item

abstract class Vehicle(): Iconizable, Item(){
    abstract fun canMove(from:Position, to:Position):Boolean
}