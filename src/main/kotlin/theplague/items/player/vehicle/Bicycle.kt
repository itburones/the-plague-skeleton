package theplague.items.player.vehicle

import theplague.interfaces.Position
import kotlin.math.abs

class Bicycle(): Vehicle() {
    override var timesLeft: Int = 5
    override val icon: String = "\uD83D\uDEB2"

    override fun canMove(from: Position, to: Position): Boolean {
        return abs(from.y - to.y) <= 4 && abs(from.x - to.x) <= 4
    }
}