package theplague.items.player

import theplague.items.player.vehicle.OnFoot
import theplague.items.player.vehicle.Vehicle
import theplague.interfaces.IPlayer
import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import theplague.items.Item
import theplague.items.player.weapon.Hand
import theplague.items.player.weapon.Weapon

class Player(
    var position: Position,
) :IPlayer, Iconizable{
    override var turns = 0
    override var livesLeft = 15
    override val icon: String = "\uD83D\uDEB6"
    override var currentVehicle:Vehicle = OnFoot()
    override var currentWeapon:Weapon = Hand()
}