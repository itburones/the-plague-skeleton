package theplague.items.player.weapon

class Sword() : Weapon() {
    override var timesLeft: Int = -1
    override val icon: String = "\uD83D\uDDE1"
}