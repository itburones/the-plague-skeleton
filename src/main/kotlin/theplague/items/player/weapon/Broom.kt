package theplague.items.player.weapon

class Broom: Weapon() {
    override var timesLeft: Int = -1

    override val icon: String = "\uD83E\uDDF9"
}