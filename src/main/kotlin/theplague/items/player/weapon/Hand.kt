package theplague.items.player.weapon

class Hand() : Weapon() {
    override var timesLeft: Int = -1

    override val icon: String = "\uD83D\uDC46"
}