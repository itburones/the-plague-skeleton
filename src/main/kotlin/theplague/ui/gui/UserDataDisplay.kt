package theplague.ui.gui
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.text.style.TextAlign
import theplague.interfaces.*
import theplague.items.player.Player


@Composable
fun UserDataDisplay(player : Player){
    val weight = 0.25f
    Row(modifier = Modifier.padding(10.dp)){
        val vehicleTimeLeft = if(player.currentVehicle.timesLeft == -1) "infinite" else player.currentVehicle.timesLeft
        val weaponTimeLeft = if(player.currentWeapon.timesLeft == -1) "infinite" else player.currentWeapon.timesLeft
        Text("Vehicle: ${player.currentVehicle.icon} : $vehicleTimeLeft", modifier = Modifier.weight(weight), textAlign = TextAlign.Center,)
        Text("Weapon: ${player.currentWeapon.icon} : $weaponTimeLeft", modifier = Modifier.weight(weight), textAlign = TextAlign.Center,)
        Text("${player.livesLeft}❤", modifier = Modifier.weight(weight), textAlign = TextAlign.Center,)
        Text("Turns: ${player.turns}", modifier = Modifier.weight(weight), textAlign = TextAlign.Center,)
    }
}