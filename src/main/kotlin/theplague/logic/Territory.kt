package theplague.logic

import kotlinx.coroutines.flow.merge
import theplague.interfaces.ITerritory
import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import theplague.items.Item
import theplague.items.colonization.colony.Ant
import theplague.items.colonization.colony.Colony
import theplague.items.colonization.colony.Dragon
import theplague.items.player.Player
import theplague.items.player.weapon.Sword
import theplague.items.player.weapon.Weapon


class Territory(val position: Position):ITerritory {
    var colony: Colony? = null
    var player: Player? = null
    var item : Item? = null

    override fun iconList(): List<Iconizable> {

        val colonyList = colony?.let { colony -> List<Iconizable>(colony.size){colony} } ?: emptyList()
        val player = player?.let { player -> listOf(player) } ?: emptyList()
        val itemList = item?.let { item -> listOf(item) } ?: emptyList()

        return colonyList + player + itemList
    }

    fun exterminate(weapon: Weapon){
        if(colony != null){
            colony!!.attacked(weapon)
            if(colony!!.size!! <= 0){
                colony = null
            }
        }
    }
}