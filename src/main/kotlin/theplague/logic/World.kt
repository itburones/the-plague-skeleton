package theplague.logic

import androidx.compose.foundation.gestures.DraggableState
import theplague.interfaces.*
import theplague.items.colonization.Colonization
import theplague.items.colonization.colony.Ant
import theplague.items.colonization.colony.Dragon
import theplague.items.player.Player
import theplague.items.player.vehicle.Bicycle
import theplague.items.player.vehicle.Helicopter
import theplague.items.player.vehicle.OnFoot
import theplague.items.player.vehicle.Vehicle
import theplague.items.player.weapon.Broom
import theplague.items.player.weapon.Hand
import theplague.items.player.weapon.Sword
import theplague.items.player.weapon.Weapon


class World(
    override val width: Int,
    override val height: Int,
): IWorld {

    override var territories: MutableList<MutableList<Territory>> = MutableList(width+1){ y -> MutableList(height+1){ x -> Territory(Position(x,y))} }
    override var player: Player = Player(Position(width/2, height/2))

    private val currentTerritory : Territory
        get() = territories[player.position.y][player.position.x]

    init{
        territories[width/2][height/2].player = player
        generateNewItems()
    }

    override fun nextTurn() {
        reproduceOrExpand()
        generateNewColonies()
        generateNewItems()
        player.turns++
    }

    override fun gameFinished(): Boolean {
        return player.livesLeft <= 0
    }

    override fun canMoveTo(position: Position): Boolean {
        return player.currentVehicle.canMove(player.position, position)
    }

    override fun moveTo(position: Position) {
        if(canMoveTo(position)){
            currentTerritory.player = null
            player.position.x = position.x
            player.position.y = position.y
            currentTerritory.player = player
            if(player.currentVehicle !is OnFoot){
                player.currentVehicle.use()
                if(player.currentVehicle.timesLeft == 0){
                    player.currentVehicle = OnFoot()
                }
            }
        }

    }

    override fun exterminate() {
        currentTerritory.exterminate(player.currentWeapon)

        /* When weapons has not infinite uses

        if(player.currentWeapon !is Hand){
            player.currentWeapon.use()
            if(player.currentWeapon.timesLeft == 0){
                player.currentWeapon = Hand()
            }
        }

        */
    }

    override fun takeableItem(): Iconizable? {
        return currentTerritory.item
    }

    override fun takeItem() {
        val item = takeableItem()
        if(item != null) {
            when (item) {
                is Weapon -> player.currentWeapon = item
                else -> player.currentVehicle = item as Vehicle
            }
        }
        currentTerritory.item = null
    }

    private fun randomPosition():Position{
        val y = (0 until height).random()
        val x = (0 until width).random()
        return Position(x,y)
    }

    private fun generateNewColonies(){
        val position = randomPosition()
        val territory = territories[position.y][position.x]
        if(territory.colony == null){
            when((1..10).random()){
                1 ->territory.colony = Dragon()
                in 2..4 -> territory.colony = Ant()
            }

        }
    }

    private fun generateNewItems(){
        val position = randomPosition()
        val territory = territories[position.y][position.x]
        if(territory.item == null){
            when((1..100).random()){
                in 1..25 ->territory.item = Bicycle()
                in 26..35 -> territory.item = Helicopter()
                in 36..60 -> territory.item = Broom()
                in 61..70 -> territory.item = Sword()
            }
        }
    }

    private fun place(colonization: Colonization){
        val colony = colonization.colony.colonizedBy(territories[colonization.position.y][colonization.position.x].colony)
        territories[colonization.position.y][colonization.position.x].colony = colony
    }

    private fun reproduce(territory: Territory){
        territory.colony!!.reproduce()
    }

    private fun expand(territory: Territory){

        val territoriesAround = territory.colony!!.expand(territory.position, Position(width,height))
        val colonizableList = mutableListOf<Colonization>()

        territoriesAround.forEach {
            if(territory.colony!!.colonizableTo(territories[it.position.y][it.position.x])){
                colonizableList.add(it)
            }
        }
        if(colonizableList.isNotEmpty()){
            val randomKey = (colonizableList.indices).random()
            place(colonizableList[randomKey])
        }
        player.livesLeft--
    }

    private fun reproduceOrExpand(){
        for(row in territories){
            for(territory in row){
                if(territory.colony != null){
                    if(territory.colony!!.needsToExpand()){
                        expand(territory)
                    }else if(territory.colony!!.willReproduce()){
                        reproduce(territory)
                    }
                }
            }
        }
    }



}